/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Files {

	private static final Logger LOGGER = LoggerFactory.getLogger(Files.class);

	public static String readString(String path) throws TemplateFileNotFoundException, TemplateFileAccessException {
		try {
			File file = new File(path);
			FileInputStream is = new FileInputStream(file);
			return readInputStream(is);
		} catch (NullPointerException e) {
			throw new TemplateFileNotFoundException(path + " file not found.", e);
		} catch (FileNotFoundException e) {
			throw new TemplateFileNotFoundException(path + " file not found.", e);
		} catch (Exception e) {
			throw new TemplateFileAccessException("Problem with " + path + " file.", e);
		}
	}

	public static String readInputStream(InputStream in) throws Exception {
		String line;
		StringBuilder out = new StringBuilder();

		BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));

		while ((line = br.readLine()) != null) {
			out.append(line).append(Constants.EOLN);
		}

		br.close();
		return out.toString();
	}

	public static void writeString(File file, String fragment) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		bw.write(fragment);
		bw.close();
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
	 *
	 * @param packageName The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static Class<?>[] getClasses(String packageName) throws ClassNotFoundException, IOException {

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		// Note: this delimiter "/" is intentional because File.separator does not work on Windows!
		String path = packageName.replace(".", "/");
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirsOrJars = new ArrayList<File>();

		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			// or jars
			dirsOrJars.add(new File(resource.getFile()));
		}
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirsOrJars) {
			if (directory.isDirectory()) {
				classes.addAll(findClassesInDir(directory, packageName));
			} else {
				classes.addAll(findClassesInJar(directory, packageName));
			}
		}
		return classes.toArray(new Class<?>[classes.size()]);
	}

	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 *
	 * @param directory   The base directory
	 * @param packageName The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	public static List<Class<?>> findClassesInDir(File directory, String packageName) throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();

		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClassesInDir(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}

	/**
	 * Looks up package for classes in the jar
	 *
	 * @param jarFile
	 * @param packageName
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */
	public static List<Class<?>> findClassesInJar(File jarFile, String packageName) throws FileNotFoundException,
		IOException, ClassNotFoundException {

		List<Class<?>> classes = new ArrayList<Class<?>>();

		// there can be full path with ! as splitter
		String jarName = jarFile.getPath();
		jarName = jarName.startsWith("file:") ? jarName.split("file:")[1] : jarName;
		jarName = jarName.contains("!") ? jarName.split("!")[0] : jarName;

		packageName = packageName.replaceAll("\\.", "/");

		LOGGER.trace("Jar '{}' looking for '{}'", jarName, packageName);

		FileInputStream fileInputStream = null;
		JarInputStream jarIS = null;
		try {
			fileInputStream = new FileInputStream(jarName);
			jarIS = new JarInputStream(fileInputStream);
			JarEntry jarEntry = null;

			// all entries
			while ((jarEntry = jarIS.getNextJarEntry()) != null) {
				// all under the path
				String resourceName = jarEntry.getName();
				if ((resourceName.startsWith(packageName)) && (resourceName.endsWith(".class"))) {
					classes.add(Class.forName(resourceName.substring(0, resourceName.length() - 6).replaceAll("/", "\\.")));
					LOGGER.trace("Found '{}'.", resourceName.replaceAll("/", "\\."));
				}
			}
			return classes;
		} finally {
			if (jarIS != null) {
				try {
					jarIS.close();
				} catch (Exception e) {
				}
			}
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (Exception e) {
				}
			}
		}
	}
}

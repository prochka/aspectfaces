/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

public class MetaProperty {

	private MetaEntity metaEntity;
	private String returnType;
	private String name;
	private Double order;
	private String specificTag;
	private boolean applicable;

	private final Map<String, Variable> variables;
	private final List<String> evaluableStrings;

	public MetaProperty(MetaEntity metaEntity) {
		this.metaEntity = metaEntity;
		this.metaEntity.getPropertySet().add(this);
		this.variables = new LinkedHashMap<String, Variable>();
		this.evaluableStrings = new ArrayList<String>();
	}

	public MetaProperty(MetaEntity metaEntity, String returnType, String name, Double order, String specificTag) {
		this(metaEntity);
		this.returnType = returnType;
		this.name = name;
		this.order = order;
		this.specificTag = specificTag;
	}

	public MetaEntity getMetaEntity() {
		return metaEntity;
	}

	public void setMetaEntity(MetaEntity metaEntity) {
		this.metaEntity = metaEntity;
	}

	/**
	 * @return entity name + "." + property name
	 */
	public String getVariableName() {
		return this.metaEntity.getName() + "." + this.getName();
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getOrder() {
		return order;
	}

	public void setOrder(Double order) {
		this.order = order;
	}

	public String getSpecificTag() {
		return specificTag;
	}

	public void setSpecificTag(String specificTag) {
		this.specificTag = specificTag;
	}

	public List<Variable> getConfigVariables() {
		List<Variable> list = new ArrayList<Variable>();
		for (Map.Entry<String, Variable> element : variables.entrySet()) {
			if (element.getValue().isConfig()) {
				list.add(element.getValue());
			}
		}
		return list;
	}

	public List<Variable> getTemplateVariables() {
		List<Variable> list = new ArrayList<Variable>();
		for (Map.Entry<String, Variable> element : variables.entrySet()) {
			if (element.getValue().isTemplate()) {
				list.add(element.getValue());
			}
		}
		return list;
	}

	public void addVariable(Variable var) {
		Variable old = this.variables.get(var.getName());
		if (old == null || old.isRedefinable()) {
			this.variables.put(var.getName(), var);
		}
	}

	public Variable getVariable(String name) {
		return this.variables.get(name);
	}

	public void addEvaluableString(String evaluableString) {
		this.evaluableStrings.add(evaluableString);
	}

	public void addVariables(List<Variable> vars) {
		for (Variable var : vars) {
			this.addVariable(var);
		}
	}

	public List<String> getEvaluableStrings() {
		return this.evaluableStrings;
	}

	public boolean isApplicable() {
		return applicable;
	}

	public void setApplicable(boolean applicable) {
		this.applicable = applicable;
	}
}

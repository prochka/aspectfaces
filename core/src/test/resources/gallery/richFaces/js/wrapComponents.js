/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * checks two inputSecrets to be the same
 * exclusive - is validator alone?
 */
function checkPassword(component, exclusive, message, allowSpaces, noSpaceMessage) {
    /* this component id to compose the new one*/
    var id = component.id;

    var outMessage = (message == null) ? Const.MSG_PASSWORD : message;
    var outNoSpaceMessage = (noSpaceMessage == null) ? Const.MSG_PASSWORD_NOSPACE : noSpaceMessage;

    /* not allow spaces */
    var allowSpaces = (allowSpaces == null) ? false : allowSpaces;
    if (!allowSpaces) {
        if (component.value.startsWith(" ") || component.value.endsWith(" ")) {
            eraseError(component);
            addError(component, outNoSpaceMessage);
            return;
        }

    }


    if (!exclusive && findValue(component).parentNode.className.match(/ errors/)) {
        /*previous component drives erase - handle first prev component*/
        return;
    }

    if (id.match(/Confirm$/)) {
        /* second - empty input*/
        /* message */
        var hint = document.getElementById(id.substring(0, id.length - 7) + "Hint");
        if (hint != null) {
            hint.firstChild.nodeValue = "modified";
        }

        var toggle = document.getElementById(id.substring(0, id.length - 7) + "Toggle_header");
        if (toggle != null) {
            toggle.style.display = 'none';
        }
        var componentToConfirm = document.getElementById(id.substring(0, id.length - 7));

        eraseError(component);
        if (component.value != componentToConfirm.value) {
            addError(component, outMessage);
        }
    } else {
        /* first - bind input */
        var hint = document.getElementById(id + "Hint");
        if (hint != null) {
            hint.firstChild.nodeValue = "modified";
        }

        var toggle = document.getElementById(id + "Toggle_header");
        if (toggle != null) {
            toggle.style.display = 'none';
        }
        var componentToConfirm = document.getElementById(id + "Confirm");

        eraseError(componentToConfirm);
        if (component.value != componentToConfirm.value) {
            addError(componentToConfirm, outMessage);
        }
    }


}
/**
 * dynamic link from current value
 */
function quickLink(inputId, link) {
    var linkId = link.id;
    var idTree = linkId.substring(0, linkId.length - ("link" + inputId).length);
    var input = document.getElementById(idTree + inputId);
    if (input.value.indexOf('://') != -1) {
        link.href = input.value;
    } else {
        link.href = "http://" + input.value;
    }
}

/**
 * password component
 */
function togglePassword(component, relativeId) {
    toggleElement(getAbsoluteId(component, relativeId));
    flipHiddenToPassword(getAbsoluteId(component, relativeId), getAbsoluteId(component, relativeId + "Hidden"));
    toggleElement(getAbsoluteId(component, relativeId) + "Confirm");
    flipHiddenToPassword(getAbsoluteId(component, relativeId) + "Confirm", getAbsoluteId(component, relativeId + "Hidden"));
    toggleElement(getAbsoluteId(component, relativeId) + "Show");
    toggleElement(relativeId + "Hint");
    return false;
}
function flipPasswordToHidden(absoluteId) {
    var object = document.getElementById(absoluteId);
    if (object.type.toLowerCase() == "password") {
        object.type = "hidden";
    }
}

function flipHiddenToPassword(objectId, copyParamsFromId) {
    var object = document.getElementById(objectId);
    var objectCopy = document.getElementById(copyParamsFromId);
    if (object.type.toLowerCase() == "hidden") {

        var newPassword = objectCopy.cloneNode(false);
        newPassword.type = "password";
        newPassword.readOnly = false;
        newPassword.style.display = "inline";
        newPassword.id = object.id;
        newPassword.name = object.name;
        object.parentNode.replaceChild(newPassword, object);

    }
}

/*
 * Toggle ID element
 */
function toggleElement(objectId) {
    var object = document.getElementById(objectId);

    if (object == null) {

    } else if (object.style.display == "") {
        object.style.display = "block";
    } else {
        object.style.display = (object.style.display != "none") ? "none" : "block";
        /* hack for ToggleBling */
        if (object.style.display == "block") {
            object.style.overflow = "visible";
        }
    }
    return false;
}

/**
 *
 *   Tag inputHtml
 *
 */
function displayHTML(component, title) {
    var win = window.open('', 'HTML preview', 'width=500,height=500');
    if (window.focus) {
        win.focus();

        win.screenX = window.screenX + 200;
        win.screenY = window.screenY + 200;
    }

    win.document.write('<html><head><title>' + title + '</title>');
    win.document.title = title;
    win.document.write('</head><body>');
    win.document.write("<div style='height:85%'>");

    var id;
    if (component.id != null && component.id.length > 4) {
        id = trimAbsolueId(component, "Html");
    } else {
        win.document.close();
        return;
    }

    var element = document.getElementById(id);
    if (element == null) {
        win.document.write("Error: element " + id + " not found");
    } else {
        win.document.write(element.value);
    }

    win.document.write("</div>");
    win.document.write("<div style='text-align:center; border-top: 1px solid #ccc; padding: 0.3em;'>")
    win.document.write("<div style='display:inline'>")
    win.document.write("<input type='button' value='Close Window' onClick='window.close()'>");
    win.document.write("</div>");
    win.document.write("</div>");
    /*
     win.document.write("<script language='JavaScript1.2'>");
     win.document.write("function winShake(milisec, movedX, movedY) {");
     win.document.write("milisec = milisec != null ? milisec : 1000;");
     win.document.write("var boundary = 50;");
     win.document.write("var offset = 3;");
     win.document.write("movedX = movedX != null ? movedX : 0;");
     win.document.write("movedY = movedY != null ? movedY : 0;");
     win.document.write("var offsetX = ((parseInt(movedX / boundary) % 2) == 0) ? offset : -offset;");
     win.document.write("var offsetY = ((parseInt(movedY / boundary) % 2) == 0) ? offset : -offset;");
     win.document.write("window.moveBy(offsetX, offsetY);");
     win.document.write("if(milisec > 0) {");
     win.document.write("setTimeout('winShake('+(milisec-3)+','+(movedX+3)+','+(movedY+3)+')', 5  );");
     win.document.write("}");
     win.document.write("}");
     win.document.write("winShake(2000);");
     */
    win.document.write("</script>");
    win.document.write("</body>");
    win.document.close();
    win.focus();


}

function circleThereAndBack(absoluteId, milisec) {
    var i;
    var radius = 50;
    var i = (milisec > 500) ? milisec % 500 : -milisec % 500;

    /* Draw a vertex on the edge of the circle. */
    var angle = Math.PI * 2 * i / 500;
    var a = radius * Math.cos(angle);
    var b = radius * Math.sin(angle);
    var component = document.getElementById(absoluteId);

    component.style.position = "relative";
    component.style.left = a + "px";
    component.style.top = b + "px";

    if (milisec > 0) {
        setTimeout("circleThereAndBack('" + absoluteId + "'," + (milisec - 3) + ")", 10);
    } else {
        component.style.position = "static";
        component.style.left = null;
        component.style.top = null;
    }


}
function spiral(absoluteId, milisec) {
    var i;
    var radius = 25 * milisec / 500;
    var i = milisec % 500;


    /* Draw a vertex on the edge of the circle. */
    var angle = Math.PI * 2 * i / 500;
    var a = radius * Math.cos(angle);
    var b = radius * Math.sin(angle);
    var component = document.getElementById(absoluteId);

    component.style.position = "relative";
    component.style.left = a + "px";
    component.style.top = b + "px";

    if (milisec > 0) {
        setTimeout("spiral('" + absoluteId + "'," + (milisec - 10) + ")", 10);
    } else {
        component.style.position = "static";
        component.style.left = null;
        component.style.top = null;
    }


}


function _shake(absoluteId, milisec, movedX) {
    milisec = milisec != null ? milisec : 2000;
    var boundary = 50;
    var offset = 3;

    /* let right*/
    movedX = movedX != null ? movedX : boundary / 2;

    var offsetX = ((parseInt(movedX / boundary) % 2) == 0) ? offset : -offset;
    /*var offsetY = ((parseInt(movedY / boundary) % 2) == 0) ? offset : -offset;*/

    var component = document.getElementById(absoluteId);

    component.style.position = "relative";

    var currentPosition = 0;
    if (component.style.left != "") {
        currentPosition = parseInt(component.style.left.substring(0, component.style.left.length));
    }
    /* left right*/
    component.style.left = (offsetX + currentPosition) + "px";


    if (milisec > 0) {
        setTimeout("_shake('" + absoluteId + "'," + (milisec - 3) + "," + (movedX + 1) + ")", 10);
    } else {
        component.style.position = "static";
        component.style.left = null;
        component.style.top = null;
    }

}


function _winShake(absoluteId, milisec, movedX, movedY) {
    milisec = milisec != null ? milisec : 2000;
    var boundary = 180;
    var offset = 3;
    var aplitude = 50;


    movedX = movedX != null ? movedX : 0;


    var offsetX = offset;

    var component = document.getElementById(absoluteId);

    component.style.position = "relative";

    var currentPosition = 0;
    if (component.style.left != "") {
        currentPosition = parseInt(component.style.left.substring(0, component.style.left.length));
    }
    /* left right
     component.style.left = (offsetX+currentPosition)+"px";*/

    component.style.top = aplitude * Math.sin(Math.PI / 180 * (offsetX + currentPosition)) + "px";
    component.style.left = aplitude * Math.cos(Math.PI / 180 * (offsetX + currentPosition)) + "px";


    if (milisec > 0) {
        setTimeout("winShake('" + absoluteId + "'," + (milisec - 3) + "," + (movedX + 1) + "," + (movedY + 1) + ")", 10);
    } else {
        component.style.position = "static";
        component.style.left = null;
        component.style.top = null;
    }
}

/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.cache;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Cache for AF resources such as tags.
 */
public class ResourceCache implements Serializable {

	private static final long serialVersionUID = -8634567942990851446L;

	Map<String, StringBuilder> templates = new HashMap<String, StringBuilder>();
	Map<String, List<Method>> classMethods = new HashMap<String, List<Method>>();
	Map<String, Annotation[]> methodAnnotations = new HashMap<String, Annotation[]>();
	Map<String, List<Method>> annotationMethods = new HashMap<String, List<Method>>();

	private static final ReentrantLock lock = new ReentrantLock();
	private static ResourceCache instance;

	public synchronized void putTemplate(String key, StringBuilder value) {
		templates.put(key, value);
	}

	public StringBuilder getTemplate(String key) {
		return templates.get(key);
	}

	public boolean containsTemplate(String key) {
		return templates.containsKey(key);
	}

	public synchronized void putClassMethods(String key, List<Method> value) {
		classMethods.put(key, value);
	}

	public List<Method> getClassMethods(String key) {
		return classMethods.get(key);
	}

	public boolean containsClassMethods(String key) {
		return classMethods.containsKey(key);
	}

	public synchronized void putMethodAnnotations(String key, Annotation[] value) {
		methodAnnotations.put(key, value);
	}

	public Annotation[] getMethodAnnotations(String key) {
		return methodAnnotations.get(key);
	}

	public boolean containsMethodAnnotations(String key) {
		return methodAnnotations.containsKey(key);
	}

	public synchronized void putAnnotationMethods(String key, List<Method> value) {
		annotationMethods.put(key, value);
	}

	public List<Method> getAnnotationMethods(String key) {
		return annotationMethods.get(key);
	}

	public boolean containsAnnotationMethods(String key) {
		return annotationMethods.containsKey(key);
	}

	public void clear() {
		templates.clear();
		classMethods.clear();
		methodAnnotations.clear();
		annotationMethods.clear();
	}

	/**
	 * @return AnnotationContainer singleton instance
	 */
	public static ResourceCache getInstance() {
		if (instance == null) {
			lock.lock();
			try {
				if (instance == null) {
					instance = new ResourceCache();
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}
}

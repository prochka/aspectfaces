/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.EvaluableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.OrderJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.SecurityJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.exceptions.AnnotationDescriptorNotFoundException;
import com.codingcrayons.aspectfaces.util.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VarLister {

	private static final Logger LOGGER = LoggerFactory.getLogger(VarLister.class);
	private static boolean FULL = true;

	public static void registerAllAnnotations() throws AnnotationDescriptorNotFoundException {

		String packageName = "";

		try {
			for (Class<?> clazz : Files.getClasses("com.codingcrayons.aspectfaces.annotation.descriptors")) {
				boolean dummyChecker = false;
				for (Class<?> cinterface : clazz.getInterfaces()) {
					if (cinterface.getName().equals(
						"com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor")) {
						dummyChecker = true;
					}
				}
				if (!dummyChecker) {
					continue;
				}
				@SuppressWarnings("unchecked")
				Class<AnnotationDescriptor> fieldDescriptorClass = (Class<AnnotationDescriptor>) clazz;

				try {
					StringBuilder builder = new StringBuilder();

					AnnotationDescriptor descriptor = fieldDescriptorClass.newInstance();
					AnnotationProvider provider = new AnnotationProvider("Dummy");

					String currentPackageName = descriptor.getAnnotationName().substring(0,
						descriptor.getAnnotationName().lastIndexOf("."));
					if (!packageName.equals(currentPackageName)) {
						packageName = currentPackageName;
						builder.append('\n');
						builder.append(currentPackageName).append('\n');
						builder.append('\n');
					}

					List<Variable> vars = new ArrayList<Variable>();
					if (descriptor instanceof VariableJoinPoint) {
						vars = ((VariableJoinPoint) descriptor).getVariables(provider);
					}
					String evalValue = "";
					if (descriptor instanceof EvaluableJoinPoint) {
						evalValue = ((EvaluableJoinPoint) descriptor).getEvaluableValue(provider);
					}
					String order = "";
					if (descriptor instanceof OrderJoinPoint) {
						order = "order";
					}
					String role = "";
					if (descriptor instanceof SecurityJoinPoint) {
						role = "roles";
					}
					builder.append("@").append(descriptor.getAnnotationName().substring(descriptor.getAnnotationName().lastIndexOf(".") + 1)).append(':');
					builder.append(fieldDescriptorClass.getSimpleName()).append(':');
					if (!vars.isEmpty()) {
						builder.append("variables=");
						for (int i = 0; i < vars.size(); ++i) {
							builder.append("").append(vars.get(i).getName());
							if (i < vars.size() - 1) {
								builder.append(",");
							}
						}
						builder.append(':');
					} else {
						builder.append(':');
					}
					if (evalValue != null && !evalValue.isEmpty()) {
						builder.append("evaluable variables=").append(evalValue).append(':');
					} else {
						builder.append(':');
					}
					if (order != null && !order.isEmpty()) {
						builder.append("order=").append(order).append(':');
					} else {
						builder.append(':');
					}
					if (role != null && !role.isEmpty()) {
						builder.append("roles=").append(role).append(':');
					} else {
						builder.append(':');
					}
					if (FULL) {
						builder.append(descriptor.getAnnotationName()).append(':');
						builder.append(fieldDescriptorClass.getName()).append(':');
					}
					builder.append('\n');
					LOGGER.trace(builder.toString());
				} catch (InstantiationException e) {
					throw new AnnotationDescriptorNotFoundException("Can't instantiate " + fieldDescriptorClass.getName(), e);
				} catch (IllegalAccessException e) {
					throw new AnnotationDescriptorNotFoundException("A constructor " + fieldDescriptorClass.getName() + " is not accessible", e);
				}
			}
		} catch (ClassNotFoundException e) {
			throw new AnnotationDescriptorNotFoundException(e);
		} catch (IOException e) {
			throw new AnnotationDescriptorNotFoundException(e);
		}
	}

	public static void main(String[] args) throws AnnotationDescriptorNotFoundException {
		registerAllAnnotations();
	}
}

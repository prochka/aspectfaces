/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.registration.behavior;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.ExtendedVariableValue;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TemplateVariableValueTest {

	private ExtendedVariableValue value;

	@Test
	public void testGetValue() {
		String stringValue = "value";
		Variable var = new Variable(stringValue, stringValue);
		value = new ExtendedVariableValue(var);
		assertEquals(value.getValue(), stringValue);
	}

	@Test
	public void testToString() {
		String stringValue = "value";
		Variable var = new Variable(stringValue, stringValue);
		value = new ExtendedVariableValue(var);
		assertEquals(value.toString(), stringValue);
	}

	@Test
	public void testFirstToUpper() {
		String expected = "Value";
		String stringValue = "value";
		Variable var = new Variable(stringValue, stringValue);
		value = new ExtendedVariableValue(var);
		assertEquals(value.firstToUpper(), expected);
		assertEquals(value.getValue(), stringValue);
	}

	@Test
	public void testFirstToLower() {
		String expected = "vALUE";
		String stringValue = "VALUE";
		Variable var = new Variable(stringValue, stringValue);
		value = new ExtendedVariableValue(var);
		assertEquals(value.firstToLower(), expected);
		assertEquals(value.getValue(), stringValue);
	}

	@Test
	public void testEscapeExpression() {
		String expected = "\\\\\\\\value";
		String stringValue = "\\value";
		Variable var = new Variable(stringValue, stringValue);
		value = new ExtendedVariableValue(var);
		assertEquals(value.escapeExpression(), expected);
	}

	@Test
	public void testShortClassNameSimpleName() {
		String className = "Foo";
		Variable var = new Variable(className, className);
		value = new ExtendedVariableValue(var);
		assertEquals(value.shortClassName().toString(), className);
	}

	@Test
	public void testShortClassNamePackageName() {
		String className = "bar.Foo";
		Variable var = new Variable(className, className);
		value = new ExtendedVariableValue(var);
		assertEquals(value.shortClassName().toString(), "Foo");
	}

	@Test
	public void testEquals() {
		Variable var = new Variable("value", "value");
		value = new ExtendedVariableValue(var);
		assertTrue(value.equals(new ExtendedVariableValue(var)));
	}

	@Test
	public void testEqualsNull() {
		Variable var = new Variable("value", "value");
		value = new ExtendedVariableValue(var);
		assertFalse(value.equals(null));
	}

	@Test
	public void testEqualsNullValueOther() {
		Variable var = new Variable("value", "value");
		value = new ExtendedVariableValue(var);
		assertFalse(value.equals(new ExtendedVariableValue(null)));
	}

	@Test
	public void testEqualsNullValue() {
		value = new ExtendedVariableValue(null);
		Variable var = new Variable("value", "value");
		assertFalse(value.equals(new ExtendedVariableValue(var)));
	}

	@Test
	public void testEqualsBothNull() {
		value = new ExtendedVariableValue(null);
		assertTrue(value.equals(new ExtendedVariableValue(null)));
	}

	@Test
	public void testEqualsObject() {
		Variable var = new Variable("value", "value");
		value = new ExtendedVariableValue(var);
		assertFalse(value.equals(new Object()));
	}

	@Test
	public void testEqualsDifferentValue() {
		Variable var = new Variable("value", "value");
		value = new ExtendedVariableValue(var);
		Variable var2 = new Variable("value", "value");
		ExtendedVariableValue value2 = new ExtendedVariableValue(var2);
		assertTrue(value.equals(value2));
		Variable var3 = new Variable("value", "zvalue");
		ExtendedVariableValue value3 = new ExtendedVariableValue(var3);
		assertFalse(value.equals(value3));
	}
}

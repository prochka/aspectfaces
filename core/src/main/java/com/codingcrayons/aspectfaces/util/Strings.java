/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

public class Strings {

	public static String lowerFirstLetter(String text) {
		return Character.toLowerCase(text.charAt(0)) + text.substring(1);
	}

	public static String upperFirstLetter(String text) {
		return Character.toUpperCase(text.charAt(0)) + text.substring(1);
	}

	// TODO From Apache Commons
	public static boolean isEmpty(String string) {
		return string == null || string.length() == 0;
	}

	// TODO From Apache Commons
	public static boolean isNotEmpty(String string) {
		return !Strings.isEmpty(string);
	}

	// TODO From Apache Commons
	public static boolean isBlank(String string) {
		int len;
		if (string == null || (len = string.length()) == 0) {
			return true;
		}

		for (int i = 0; i < len; i++) {
			if (!Character.isWhitespace(string.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	// TODO From Apache Commons
	public static boolean isNotBlank(String string) {
		return !Strings.isBlank(string);
	}
}

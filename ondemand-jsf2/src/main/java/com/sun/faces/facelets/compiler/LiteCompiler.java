/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sun.faces.facelets.compiler;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletException;
import javax.faces.view.facelets.FaceletHandler;
import javax.faces.view.facelets.TagDecorator;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.sun.faces.RIConstants;
import com.sun.faces.facelets.LiteCache;
import com.sun.faces.facelets.tag.TagLibrary;
import com.sun.faces.util.Util;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class LiteCompiler extends Compiler {

	private SAXCompiler delegate = null;

	public LiteCompiler(SAXCompiler c) {
		super();
		super.setTrimmingComments(true);
		super.setTrimmingWhitespace(true);
		this.delegate = c;

		// copy these from the delegate
		for (TagLibrary lib : getLibraries(c)) {
			super.addTagLibrary(lib);
		}
		for (TagDecorator dec : getDecorators(c)) {
			super.addTagDecorator(dec);
		}
		for (Map.Entry<String, String> feature : getFeatures(c).entrySet()) {
			super.setFeature(feature.getKey(), feature.getValue());
		}
	}

	@Override
	protected FaceletHandler doCompile(URL src, String alias) throws IOException, ELException, FacesException {
		return delegate.doCompile(src, alias);
	}

	private Object makeCompilationManager(String alias, Compiler compiler) {
		try {
			Constructor<?> constructor = LiteCache.getConstructor("com.sun.faces.facelets.compiler.CompilationManager");
			if (constructor == null) {
				Class<?> cm = Class.forName("com.sun.faces.facelets.compiler.CompilationManager");
				Class<?> partypes[] = {String.class, Compiler.class};

				constructor = cm.getConstructor(partypes);
				constructor.setAccessible(true);
				LiteCache.putConstructor("com.sun.faces.facelets.compiler.CompilationManager", constructor);
			}

			Object arglist[] = {alias, compiler};
			return constructor.newInstance(arglist);
		} catch (Exception e) {
			throw new FacesException("CompilationManager not found.");
		}
	}

	private Object makeCompilationHandler(Object mngr, String alias) {

		try {
			Constructor<?> constructor = LiteCache.getConstructor("com.sun.faces.facelets.compiler.SAXCompiler$CompilationHandler");
			if (constructor == null) {
				Class<?> cm = Class.forName("com.sun.faces.facelets.compiler.SAXCompiler$CompilationHandler");
				Class<?> partypes[] = {mngr.getClass(), String.class};

				constructor = cm.getConstructor(partypes);
				constructor.setAccessible(true);
				LiteCache.putConstructor("com.sun.faces.facelets.compiler.SAXCompiler$CompilationHandler", constructor);
			}

			Object arglist[] = {mngr, alias};
			return constructor.newInstance(arglist);
		} catch (Exception e) {
			throw new FacesException("CompilationHandler not found.");
		}
	}

	public FaceletHandler compile(InputStream is, URL url, String alias)
		throws IOException, ELException, FacesException {
		return this.doCompile(is, url, alias);
	}

	protected FaceletHandler doCompile(InputStream is, URL url, String alias)
		throws IOException, ELException, FacesException {
		Object mngr = null;
		String encoding = getEncoding();

		try {
			mngr = makeCompilationManager(alias, this);
			// mngr = new CompilationManager(alias, this);
			writeXmlDecl(is, encoding, mngr);

			Object handler = makeCompilationHandler(mngr, alias);
			SAXParser parser = this.createSAXParser(handler);
			parser.parse(is, (DefaultHandler) handler);
		} catch (SAXException e) {
			throw new FaceletException("Error Parsing " + alias + ": " + e.getMessage(), e.getCause());
		} catch (ParserConfigurationException e) {
			throw new FaceletException("Error Configuring Parser " + alias + ": " + e.getMessage(), e.getCause());
		} finally {
			if (is != null) {
				is.close();
			}
		}
		FaceletHandler result = new EncodingHandler(createFaceletHandler(mngr), encoding,
			getCompilationMessageHolder(mngr));
		setCompilationMessageHolder(mngr, null);
		// does not allow us to access at JBoss
		// FaceletHandler result = new EncodingHandler(mngr.createFaceletHandler(), encoding, mngr.getCompilationMessageHolder());
		// mngr.setCompilationMessageHolder(null);

		return result;
	}

	private String getEncoding() {
		String result;
		String encodingFromRequest = null;
		FacesContext context = FacesContext.getCurrentInstance();
		if (null != context) {
			ExternalContext extContext = context.getExternalContext();
			encodingFromRequest = extContext.getRequestCharacterEncoding();
		}
		result = (null != encodingFromRequest) ? encodingFromRequest : RIConstants.CHAR_ENCODING;

		return result;
	}

	private void writeXmlDecl(InputStream is, String encoding, Object mngr) {
		try {
			// does not allow us to access at JBoss
			Method method = LiteCache.getMethod("SAXCompiler#writeXmlDecl");
			if (method == null) {

				try {
					// the method API depends on Mojarra JSF impl version
					// prior to JSF 2.1.13 - old version of API, method has only 2 parameters
					// the issue is documented as AF-140, AF-159, AF-163
					method = delegate.getClass().getDeclaredMethod("writeXmlDecl", new Class<?>[]{InputStream.class, mngr.getClass()});
				} catch (NoSuchMethodException ignored) {
					// newer than JSF 2.1.13 - new version of API, method has 3 parameters
					method = delegate.getClass().getDeclaredMethod("writeXmlDecl", new Class<?>[]{InputStream.class, String.class, mngr.getClass()});
				}

				method.setAccessible(true);

				LiteCache.putMethod("SAXCompiler#writeXmlDecl", method);
			}
			// depends on Mojarra JSF impl version
			// the issue is documented as AF-140, AF-159, AF-163
			Object arglist[] = method.getParameterTypes().length == 2 ? new Object[]{is, mngr} : new Object[]{is, encoding, mngr};
			method.invoke(delegate, arglist);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private List<TagLibrary> getLibraries(Compiler c) {
		try {
			Field field = LiteCache.getField("Compiler#libraries");
			if (field == null) {
				field = c.getClass().getSuperclass().getDeclaredField("libraries");
				field.setAccessible(true);

				LiteCache.putField("Compiler#libraries", field);
			}
			return (List<TagLibrary>) field.get(c);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private List<TagDecorator> getDecorators(Compiler c) {
		try {
			Field field = LiteCache.getField("Compiler#decorators");
			if (field == null) {
				field = c.getClass().getSuperclass().getDeclaredField("decorators");
				field.setAccessible(true);

				LiteCache.putField("Compiler#decorators", field);
			}
			return (List<TagDecorator>) field.get(c);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> getFeatures(Compiler c) {
		try {
			Field field = LiteCache.getField("Compiler#features");
			if (field == null) {
				field = c.getClass().getSuperclass().getDeclaredField("features");
				field.setAccessible(true);

				LiteCache.putField("Compiler#features", field);
			}
			return (Map<String, String>) field.get(c);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	private FaceletHandler createFaceletHandler(Object mngr) {
		try {
			Method method = LiteCache.getMethod("CompilationManager#createFaceletHandler");
			if (method == null) {
				method = mngr.getClass().getMethod("createFaceletHandler");
				method.setAccessible(true);

				LiteCache.putMethod("CompilationManager#createFaceletHandler", method);
			}
			return (FaceletHandler) method.invoke(mngr);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	private CompilationMessageHolder getCompilationMessageHolder(Object mngr) {
		try {
			Method method = LiteCache.getMethod("CompilationManager#getCompilationMessageHolder");
			if (method == null) {
				method = mngr.getClass().getMethod("getCompilationMessageHolder");
				method.setAccessible(true);

				LiteCache.putMethod("CompilationManager#getCompilationMessageHolder", method);
			}
			return (CompilationMessageHolder) method.invoke(mngr);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	private void setCompilationMessageHolder(Object mngr, Object value) {
		try {
			Method method = LiteCache.getMethod("CompilationManager#setCompilationMessageHolder");
			if (method == null) {
				Class<?> partypes[] = {CompilationMessageHolder.class};
				method = mngr.getClass().getMethod("setCompilationMessageHolder", partypes);
				method.setAccessible(true);

				LiteCache.putMethod("CompilationManager#setCompilationMessageHolder", method);
			}
			Object arglist[] = {value};
			method.invoke(mngr, arglist);
		} catch (Exception e) {
			throw new FacesException(e);
		}
	}

	private final SAXParser createSAXParser(Object handler)
		throws SAXException, ParserConfigurationException {
		SAXParserFactory factory = Util.createSAXParserFactory();
		// SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		factory.setFeature("http://xml.org/sax/features/validation", this.isValidating());
		factory.setValidating(this.isValidating());
		SAXParser parser = factory.newSAXParser();
		XMLReader reader = parser.getXMLReader();
		reader.setProperty("http://xml.org/sax/properties/lexical-handler", handler);
		reader.setErrorHandler((ErrorHandler) handler);
		reader.setEntityResolver((EntityResolver) handler);

		return parser;
	}

	@Override
	protected FaceletHandler doMetadataCompile(URL src, String alias) throws IOException, ELException, FacesException {
		return delegate.doMetadataCompile(src, alias);
	}
}

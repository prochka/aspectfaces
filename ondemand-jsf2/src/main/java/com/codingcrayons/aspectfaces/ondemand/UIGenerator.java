/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.ondemand;

import java.io.IOException;

import javax.el.ELException;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.component.NamingContainer;
import javax.faces.component.StateHolder;
import javax.faces.component.UIComponentBase;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class UIGenerator extends UIComponentBase implements NamingContainer, ValueHolder, StateHolder {

	/**
	 * <p>
	 * The standard component type for this component.
	 * </p>
	 */
	public static final String COMPONENT_TYPE = "javax.faces.Output";

	/**
	 * <p>
	 * The standard component family for this component.
	 * </p>
	 */
	public static final String COMPONENT_FAMILY = "javax.faces.Output";

	private Object value;

	public UIGenerator() {
		super();
		setRendererType(null);
	}

	public boolean getRendersChildren() {
		return true;
	}

	public void encodeBegin(FacesContext facesContext) throws IOException {
	}

	@Override
	public void encodeChildren(FacesContext facesContext) throws IOException {
	}

	@Override
	public void encodeEnd(FacesContext facesContext) throws IOException {
	}

	@Override
	public String getFamily() {
		return COMPONENT_TYPE;
	}

	public Object getLocalValue() {
		return (this.value);
	}

	public Object getValue() {
		if (this.value != null) {
			return (this.value);
		}
		ValueExpression ve = getValueExpression("value");
		if (ve != null) {
			try {
				return (ve.getValue(getFacesContext().getELContext()));
			} catch (ELException e) {
				throw new FacesException(e);
			}
		} else {
			return (null);
		}
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[3];
		values[0] = super.saveState(context);
		values[1] = value;
		values[2] = getId();
		return values;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		Object values[] = (Object[]) state;
		super.restoreState(context, values[0]);
		value = values[1];
		this.setId((String) values[2]);
	}

	@Override
	public Converter getConverter() {
		return null;
	}

	@Override
	public void setConverter(Converter arg0) {
	}
}

/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.support.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotations.UiFormOrder;
import com.codingcrayons.aspectfaces.annotations.UiFormProfiles;
import com.codingcrayons.aspectfaces.annotations.UiPassword;
import com.codingcrayons.aspectfaces.annotations.UiTableProfiles;
import com.codingcrayons.aspectfaces.annotations.UiTableUserRoles;

public class Administrator implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private boolean enabled;
	private String username;
	private String passwordhash;
	private String firstname;
	private String lastname;
	private Date lastLogged;
	private Long loginCount;
	private Set<Role> roles = new HashSet<Role>(0);
	private String email;
	private Long version;

	public Administrator() {
	}

	public Administrator(Long id, boolean enabled, String username, String passwordhash, String firstname,
						 String lastname, String email) {
		this.id = id;
		this.enabled = enabled;
		this.username = username;
		this.passwordhash = passwordhash;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}

	public Administrator(Long id, boolean enabled, String username, String passwordhash, String firstname,
						 String lastname, Date lastLogged, Long loginCount, String email) {
		this.id = id;
		this.enabled = enabled;
		this.username = username;
		this.passwordhash = passwordhash;
		this.firstname = firstname;
		this.lastname = lastname;
		this.lastLogged = lastLogged;
		this.loginCount = loginCount;
		this.email = email;
	}

	@UiFormProfiles({"ffff", "password"})
	@UiTableUserRoles({"GUEST", "YYY"})
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@UiTableProfiles({"edit", "new"})
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@UiFormProfiles({"edit", "new", "search", "password"})
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@UiFormProfiles({"password"})
	@UiPassword
	public String getPasswordhash() {
		return this.passwordhash;
	}

	public void setPasswordhash(String passwordhash) {
		this.passwordhash = passwordhash;
	}

	@UiFormProfiles({"edit", "new", "search"})
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@UiFormProfiles({"edit", "new", "search"})
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@UiFormProfiles({"edit", "new"})
	public Date getLastLogged() {
		return this.lastLogged;
	}

	public void setLastLogged(Date lastLogged) {
		this.lastLogged = lastLogged;
	}

	@UiFormProfiles({"edit", "new"})
	public Long getLoginCount() {
		return this.loginCount;
	}

	public void setLoginCount(Long loginCount) {
		this.loginCount = loginCount;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@UiFormProfiles({"edit", "new", "search"})
	@UiFormOrder(1)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}

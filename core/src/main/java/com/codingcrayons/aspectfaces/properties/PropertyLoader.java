/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyLoader {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyLoader.class);
	private final Properties properties = new Properties();

	/**
	 * Creates a new property loader and loads properties from specified file.
	 *
	 * @param propertiesFile File path to file with properties.
	 */
	public PropertyLoader(String propertiesFile) throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		InputStream inputStream = this.getClass().getResourceAsStream(propertiesFile);
		try {
			if (inputStream == null) {
				LOGGER.trace("Properties file '" + propertiesFile + "' not found.");
				throw new ConfigurationFileNotFoundException("Properties file '" + propertiesFile + "' not found.");
			} else {
				loadProperties(new InputStreamReader(inputStream));
			}
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				// ignore since commons io does the same
				// See org.apache.commons.io.IOUtils.java#IOUtils.closeQuietly
				LOGGER.error("Closing property file stream", e);
			}
		}
	}

	/**
	 * Creates a new property loader and loads properties from input stream.
	 *
	 * @param inputStream Input Stream of properties file.
	 */
	public PropertyLoader(InputStream inputStream) throws ConfigurationParsingException {
		loadProperties(new InputStreamReader(inputStream));
	}

	/**
	 * Creates a new property loader and loads properties from specified reader.
	 *
	 * @param reader Reader that contains properties.
	 */
	public PropertyLoader(Reader reader) throws IOException {
		properties.load(reader);
	}

	private void loadProperties(Reader reader) throws ConfigurationParsingException {
		try {
			properties.load(reader);
		} catch (IOException e) {
			// if happens then properties will be null
			LOGGER.error("Loading property file.", e);
			throw new ConfigurationParsingException("Loading property file.", e);
		}
	}

	/**
	 * Gets a property with specified name
	 *
	 * @param name Name of property
	 * @return Property or null
	 */
	public String getProperty(String name) {
		return properties.getProperty(name);
	}

	/**
	 * Gets a property with specified name
	 *
	 * @param name         Name of property
	 * @param defaultValue Default value of property
	 * @return Property or default value
	 */
	public String getProperty(String name, String defaultValue) {
		return properties.getProperty(name, defaultValue);
	}
}

/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.descriptors;

import java.io.File;
import java.util.List;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.annotations.UiParam;
import com.codingcrayons.aspectfaces.annotations.UiParamType;
import com.codingcrayons.aspectfaces.configuration.ConfigurationStorage;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.StaticConfiguration;
import com.codingcrayons.aspectfaces.exceptions.AFException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UiParamAnnotationDescriptorTest extends TestCase {

	private static final String P_STRING = "pString";
	private static final String P_INTEGER = "pInteger";
	private static final String P_LONG = "pLong";
	private static final String P_FLOAT = "pFloat";
	private static final String P_DOUBLE = "pDouble";
	private static final String P_BOOLEAN = "pBoolean";

	private static final String V_STRING = "pString";
	private static final Integer V_INTEGER = 2;
	private static final Long V_LONG = 200l;
	private static final Float V_FLOAT = 2.15f;
	private static final Double V_DOUBLE = 2.43;
	private static final Boolean V_BOOLEAN = true;

	private static final String CONFIG = "detail.config.xml";

	private Variable pString;
	private Variable pInteger;
	private Variable pLogn;
	private Variable pFloat;
	private Variable pDouble;
	private Variable pBoolean;

	@UiParam(name = P_STRING, value = V_STRING)
	public String getPString() {
		return "";
	}

	@UiParam(name = P_INTEGER, value = "2", type = UiParamType.INTEGER)
	public String getPInteger() {
		return "";
	}

	@UiParam(name = P_LONG, value = "200", type = UiParamType.LONG)
	public String getPLong() {
		return "";
	}

	@UiParam(name = P_FLOAT, value = "2.15", type = UiParamType.FLOAT)
	public String getPFloat() {
		return "";
	}

	@UiParam(name = P_DOUBLE, value = "2.43", type = UiParamType.DOUBLE)
	public String getPDouble() {
		return "";
	}

	@UiParam(name = P_BOOLEAN, value = "true", type = UiParamType.BOOLEAN)
	public String getPBoolean() {
		return "";
	}

	@BeforeClass
	public void setUp() throws AFException {

		AFWeaver.registerAllAnnotations();

		ConfigurationStorage.getInstance().addConfiguration(
			new StaticConfiguration(CONFIG), new File(getConfig(CONFIG)), false, false
		);

		Context context = new Context("Test", null, null, null, false);
		context.setConfiguration(ConfigurationStorage.getInstance().getConfiguration(CONFIG));
		JavaInspector javaInspector = new JavaInspector(this.getClass());
		List<MetaProperty> metaProperties = javaInspector.inspect(context);

		for (MetaProperty metaProperty : metaProperties) {
			if (metaProperty.getName().equals(P_STRING)) {
				this.pString = this.getVariable(metaProperty.getConfigVariables(), P_STRING);
			} else if (metaProperty.getName().equals(P_INTEGER)) {
				this.pInteger = this.getVariable(metaProperty.getConfigVariables(), P_INTEGER);
			} else if (metaProperty.getName().equals(P_LONG)) {
				this.pLogn = this.getVariable(metaProperty.getConfigVariables(), P_LONG);
			} else if (metaProperty.getName().equals(P_FLOAT)) {
				this.pFloat = this.getVariable(metaProperty.getConfigVariables(), P_FLOAT);
			} else if (metaProperty.getName().equals(P_DOUBLE)) {
				this.pDouble = this.getVariable(metaProperty.getConfigVariables(), P_DOUBLE);
			} else if (metaProperty.getName().equals(P_BOOLEAN)) {
				this.pBoolean = this.getVariable(metaProperty.getConfigVariables(), P_BOOLEAN);
			}
		}
	}

	private Variable getVariable(List<Variable> variables, String name) {
		for (Variable variable : variables) {
			if (variable.getName().equals(name)) {
				return variable;
			}
		}
		return null;
	}

	@Test
	public void testStringParam() {
		assertTrue(this.pString.getValue() instanceof String);
		assertEquals(this.pString.getValue(), V_STRING);
	}

	@Test
	public void testIntegerParam() {
		assertTrue(this.pInteger.getValue() instanceof Integer);
		assertEquals(this.pInteger.getValue(), V_INTEGER);
	}

	@Test
	public void testLongParam() {
		assertTrue(this.pLogn.getValue() instanceof Long);
		assertEquals(this.pLogn.getValue(), V_LONG);
	}

	@Test
	public void testFloatParam() {
		assertTrue(this.pFloat.getValue() instanceof Float);
		assertEquals(this.pFloat.getValue(), V_FLOAT);
	}

	@Test
	public void testDoubleParam() {
		assertTrue(this.pDouble.getValue() instanceof Double);
		assertEquals(this.pDouble.getValue(), V_DOUBLE);
	}

	@Test
	public void testBooleanParam() {
		assertTrue(this.pBoolean.getValue() instanceof Boolean);
		assertEquals(this.pBoolean.getValue(), V_BOOLEAN);
	}
}

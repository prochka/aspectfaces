/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.composition;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.util.Collections;
import com.codingcrayons.aspectfaces.variableResolver.TagParserException;
import com.codingcrayons.aspectfaces.variableResolver.TagVariableResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codingcrayons.aspectfaces.util.Constants.V_FRAGMENT;

@SuppressWarnings("unchecked")
public class UIFragmentComposer {

	private static final Logger LOGGER = LoggerFactory.getLogger(UIFragmentComposer.class);

	private final List<MetaProperty> metaProperties;
	private final List<IterationPart> iterationParts;
	private final Pattern pattern;

	private static final String ITERATION_PART_TAG = "iteration-part";
	private static final String ITERATION_PART_FLAG = "AF-ITERATION_PART_";
	private static final String REMOVE_FLAG = "AF-REMOVE";
	private static final String COVER_TAG = "cover";
	private static final String AF_TAG = "af";
	private static final String NEXT_TAG = "next";

	private class IterationPart {
		private final String content;
		private final Integer maxOccurs;
		private int used;
		private final int number;
		private int end;

		public IterationPart(int number, String content, Integer maxOccurs, int end) {
			this.number = number;
			this.content = content;
			this.maxOccurs = maxOccurs;
			this.end = end;
		}

		public void increaseUse() {
			this.used++;
		}

		public boolean canBeUsed() {
			return this.maxOccurs == null || this.used < this.maxOccurs;
		}

		public int getNumber() {
			return this.number;
		}

		public String getContent() {
			return this.content;
		}

		public int getEnd() {
			return end;
		}

		public void setEnd(int end) {
			this.end = end;
		}
	}

	public UIFragmentComposer() {
		this.metaProperties = new ArrayList<MetaProperty>();
		this.iterationParts = new CopyOnWriteArrayList<IterationPart>();
		this.pattern = Pattern.compile("maxOccurs[\\s]*=\\s*(\"|')(\\d+)(\"|')");
	}

	public void addField(MetaProperty metaProperty) {
		this.metaProperties.add(metaProperty);
	}

	public void addAllFields(List<MetaProperty> metaProperties) {
		this.metaProperties.addAll(metaProperties);
	}

	public UIFragment composeForm(Context context) throws TemplateFileNotFoundException, TemplateFileAccessException,
		EvaluatorException, ConfigurationNotSetException, TagParserException {

		LOGGER.trace("Composing fragment: {}.", context.getFragmentName());

		UIFragment uiFragment = new UIFragment();

		if (context.getLayout() == null) {
			this.composeClassicForm(context, uiFragment);
		} else {
			this.composeLayoutedForm(context, uiFragment);
		}

		if (context.isUseCover()) {
			StringBuilder header = context.getConfiguration().getHeaderTag();
			if (header != null) {
				uiFragment.addHeader(this.createUnboundUIFragment("header", context.getConfiguration().getHeaderTag(),
					context));
			}

			StringBuilder footer = context.getConfiguration().getFooterTag();
			if (footer != null) {
				uiFragment.addFooter(this.createUnboundUIFragment("footer", context.getConfiguration().getFooterTag(),
					context));
			}
		}

		return uiFragment;
	}

	private Integer parseMaxOccurs(String iterationPart) {
		Integer maxOccurs = null;
		Matcher m = pattern.matcher(iterationPart);
		if (m.find()) {
			maxOccurs = Integer.parseInt(m.group(2));
		}
		return maxOccurs;
	}

	private String parseIterationParts(String layout) {
		int number = 0;
		while (layout.contains("<" + AF_TAG + ":" + ITERATION_PART_TAG)) {
			int start = layout.indexOf("<" + AF_TAG + ":" + ITERATION_PART_TAG);
			int end = layout.indexOf(">", layout.indexOf("</" + AF_TAG + ":" + ITERATION_PART_TAG));

			String iterationPart = layout.substring(start, end + 1);

			String iterationPartFlag = "<" + ITERATION_PART_FLAG + number + ">";
			layout = layout.substring(0, start) + iterationPartFlag + layout.substring(end + 1);

			Integer maxOccurs = this.parseMaxOccurs(iterationPart);
			iterationPart = iterationPart.replaceAll("</?" + AF_TAG + ":" + ITERATION_PART_TAG + "[^>]*>", "");

			this.iterationParts.add(new IterationPart(number++, iterationPart, maxOccurs, start
				+ iterationPartFlag.length()));
		}
		return layout;
	}

	private void shiftIterationPartsEnd(int diff, int index) {
		for (IterationPart ip : iterationParts) {
			if (ip.getEnd() > index) {
				ip.setEnd(ip.getEnd() + diff);
			}
		}
	}

	private String replaceTagForNamedFlag(String layout, int flagOccurrence, String fieldFlag, StringBuilder tag) {
		int prevLength = layout.length();

		layout = layout.replace(
			AFWeaver.getOpenVariableBoundaryIdentifier() + AF_TAG + ":" + fieldFlag
				+ AFWeaver.getCloseVariableBoundaryIdentifier(), tag).replace(
			AF_TAG + ":" + COVER_TAG + "-" + fieldFlag, REMOVE_FLAG);

		this.shiftIterationPartsEnd(layout.length() - prevLength, flagOccurrence);

		return layout;
	}

	private String composeStartVariableIdentifierForRegexp() {
		String boundary = AFWeaver.getOpenVariableBoundaryIdentifier();
		boundary = escapeUnsafeCharacters(boundary);
		return boundary;
	}

	private String composeEndVariableIdentifierForRegexp() {
		String boundary = AFWeaver.getCloseVariableBoundaryIdentifier();
		boundary = escapeUnsafeCharacters(boundary);
		return boundary;
	}

	private String escapeUnsafeCharacters(String ch) {
		return escape(ch, "$", "[", "]", "{", "(", ")", "+", ".", "|", "*", "?", "^");
	}

	private String escape(String boundary, String... toEscapeArray) {
		for (String toEscape : toEscapeArray) {
			boundary = boundary.replace(toEscape, "\\" + toEscape);
		}
		return boundary;
	}

	private String replaceTagForNextFlag(String layout, int flagOccurrence, String tag) {
		int prevLength = layout.length();

		String regexpOpenIdentifier = this.composeStartVariableIdentifierForRegexp();
		String regexpCloseIdentifier = this.composeEndVariableIdentifierForRegexp();

		tag = tag.replace("$", "\\$"); // because of Illegal group reference
		layout = layout.replaceFirst(regexpOpenIdentifier + AF_TAG + ":" + NEXT_TAG + regexpCloseIdentifier, tag)
			.replaceFirst(AF_TAG + ":" + COVER_TAG + "-" + NEXT_TAG, REMOVE_FLAG)
			.replaceFirst("/" + AF_TAG + ":" + COVER_TAG + "-" + NEXT_TAG, REMOVE_FLAG);

		this.shiftIterationPartsEnd(layout.length() - prevLength, flagOccurrence);

		return layout;
	}

	/*
	 * Tomas: VASEKFIX document this ;
	 */
	private void composeLayoutedForm(Context context, UIFragment form) throws TemplateFileNotFoundException,
		TemplateFileAccessException, EvaluatorException, ConfigurationNotSetException, TagParserException {

		String layout;
		try {
			layout = this.parseIterationParts(context.getConfiguration().getTag(context.getLayout()).toString());
		} catch (Exception e) {
			throw new TemplateFileAccessException("Cannot load layout file " + context.getLayout(), e);
		}

		Iterator<IterationPart> iterationPartsIterator = this.iterationParts.iterator();
		IterationPart currentIterationPart = null;

		boolean containsNext = true;

		for (MetaProperty property : this.getOrderedFields(this.filterApplicable(this.metaProperties), context)) {
			// Tomas : VASEKFIX would it be faster to parse all named, put them to a MAP and then lookup the MAP?
			int namedOccurrence = layout.indexOf(AFWeaver.getOpenVariableBoundaryIdentifier() + AF_TAG + ":"
				+ property.getName() + AFWeaver.getCloseVariableBoundaryIdentifier());
			if (namedOccurrence > -1) {
				layout = this.replaceTagForNamedFlag(layout, namedOccurrence, property.getName(),
					this.createBoundUIFragment(context, property));
				continue;
			}

			while (containsNext) {

				if (currentIterationPart == null || !currentIterationPart.canBeUsed()) {
					if (iterationPartsIterator.hasNext()) {
						currentIterationPart = iterationPartsIterator.next();
						continue; // Tomas: VASEKFIX why is continue here?
					} else {
						currentIterationPart = null;
					}
				}

				int nextOccurrence = layout.indexOf(AFWeaver.getOpenVariableBoundaryIdentifier() + AF_TAG + ":"
					+ NEXT_TAG + AFWeaver.getCloseVariableBoundaryIdentifier());

				// iteration part is null or tag occurrence is before this
				// iteration part
				if ((currentIterationPart == null && nextOccurrence != -1)
					|| (currentIterationPart != null && nextOccurrence != -1 && nextOccurrence < currentIterationPart
					.getEnd())) {
					layout = this.replaceTagForNextFlag(layout, nextOccurrence,
						this.createBoundUIFragment(context, property).toString());
					break;
				} else if (currentIterationPart == null) {
					// no more iteration parts and no occurrence
					containsNext = false;
					break;
				}

				layout = this.insertIterationPartToLayout(layout, currentIterationPart);
			}
		}
		form.addItem(new StringBuilder(this.cleanLayout(layout)));
	}

	private String insertIterationPartToLayout(String layout, IterationPart iterationPart) {
		if (iterationPart.canBeUsed()) {
			String ipFlag = "<" + ITERATION_PART_FLAG + iterationPart.getNumber() + ">";

			int ipFlagPosition = layout.indexOf(ipFlag);
			if (ipFlagPosition >= 0) {
				layout = layout.substring(0, ipFlagPosition) + iterationPart.getContent()
					+ layout.substring(ipFlagPosition);
				iterationPart.increaseUse();

				this.shiftIterationPartsEnd(iterationPart.getContent().length(), ipFlagPosition);
			}
		}
		return layout;
	}

	private StringBuilder createBoundUIFragment(Context context, MetaProperty property)
		throws TemplateFileNotFoundException, TemplateFileAccessException, EvaluatorException,
		ConfigurationNotSetException, TagParserException {
		String tagName;
		if (property.getSpecificTag() != null) {
			tagName = property.getSpecificTag();
		} else {
			tagName = context.getConfiguration().getTagPath(property, context);
		}
		if (tagName == null) {
			// mapping not defined
			LOGGER.info("No mapping rule found for '{}'.", property.getReturnType());
			return new StringBuilder("");
		}

		StringBuilder tag = context.getConfiguration().getTag(tagName);
		List<Variable> localVars = Collections.concatLists(context.getVariableList(), property.getTemplateVariables());
		return initUIFragment(tagName, tag, localVars);
	}

	private void composeClassicForm(Context context, UIFragment uiFragment) throws TemplateFileNotFoundException,
		TemplateFileAccessException, EvaluatorException, ConfigurationNotSetException, TagParserException {

		for (MetaProperty property : this.getOrderedFields(this.filterApplicable(this.metaProperties), context)) {
			uiFragment.addItem(this.createBoundUIFragment(context, property));
		}
	}

	private StringBuilder createUnboundUIFragment(String tagName, StringBuilder uiFragment, Context context)
		throws TemplateFileAccessException, EvaluatorException, TagParserException {

		List<Variable> localVars = Collections.concatList(context.getVariableList(),
			new Variable(V_FRAGMENT, context.getFragmentName()));
		return initUIFragment(tagName, uiFragment, localVars);
	}

	private StringBuilder initUIFragment(String tagName, StringBuilder uiFragment, List<Variable> localVars)
		throws TemplateFileAccessException, EvaluatorException, TagParserException {

		TagVariableResolver vr = new TagVariableResolver();
		try {
			uiFragment = vr.resolve(tagName, uiFragment, AFWeaver.getOpenVariableBoundaryIdentifier(),
				AFWeaver.getCloseVariableBoundaryIdentifier(), localVars);
		} catch (TagParserException e) {
			throw new EvaluatorException(e.getMessage(), e);
		}

		return uiFragment;
	}

	public List<MetaProperty> filterApplicable(List<MetaProperty> metaProperties) {
		List<MetaProperty> filtered = new ArrayList<MetaProperty>(metaProperties.size());
		for (MetaProperty metaProperty : metaProperties) {
			if (metaProperty.isApplicable()) {
				filtered.add(metaProperty);
			}
		}
		return filtered;
	}

	public List<MetaProperty> getOrderedFields(List<MetaProperty> metaProperties, final Context context) {

		Comparator<MetaProperty> comparator = new Comparator<MetaProperty>() {
			@Override
			public int compare(MetaProperty mp1, MetaProperty mp2) {
				// collate
				if (context.isCollate()) {
					// order above an entity
					int comparisonEntity = mp1.getMetaEntity().getOrder().compareTo(mp2.getMetaEntity().getOrder());
					if (comparisonEntity != 0) {
						return comparisonEntity;
					} else {
						return compareFieldOrder(mp1, mp2);
					}
				} else {
					return compareFieldOrder(mp1, mp2);
				}
			}

			private int compareFieldOrder(MetaProperty mp1, MetaProperty mp2) {
				if (mp1.getOrder() == null && mp2.getOrder() == null) {
					return mp1.getName().compareTo(mp2.getName());
				} else if (mp1.getOrder() == null) {
					return 1;
				} else if (mp2.getOrder() == null) {
					return -1;
				} else {
					int comparison = mp1.getOrder().compareTo(mp2.getOrder());
					if (comparison == 0) {
						// Collision of order
						LOGGER.warn("Colliding field order with value " + context.getOrderAnnotation() + "(" + mp2.getOrder()
							+ ") set for class: " + mp1.getMetaEntity().getName() + " fields: '" + mp1.getName()
							+ "' and '" + mp2.getName() + "'");
						return mp1.getName().compareTo(mp2.getName()); // order alphabetically
					} else {
						return comparison;
					}
				}
			}
		};

		java.util.Collections.sort(metaProperties, comparator);

		return metaProperties;
	}

	private String removePartOfString(String content, int start, int end) {
		return content.substring(0, start) + content.substring(end + 1);
	}

	private String cleanLayout(String layout) {

		String regexpOpenIdentifier = this.composeStartVariableIdentifierForRegexp();
		String regexpCloseIdentifier = this.composeEndVariableIdentifierForRegexp();

		StringBuilder rmVariable = new StringBuilder(regexpOpenIdentifier);
		rmVariable.append(AF_TAG).append(":[^").append(regexpCloseIdentifier);
		rmVariable.append("]*").append(regexpCloseIdentifier);

		layout = layout.replaceAll("</?" + REMOVE_FLAG + "\\s*>", "").replaceAll(rmVariable.toString(), "")
			.replaceAll("<" + ITERATION_PART_FLAG + "\\d+>", "");

		while (layout.contains("<" + AF_TAG + ":" + COVER_TAG + "")) {
			int start = layout.indexOf("<" + AF_TAG + ":" + COVER_TAG + "");
			int end = layout.indexOf(">", layout.indexOf("</" + AF_TAG + ":" + COVER_TAG + ""));
			layout = this.removePartOfString(layout, start, end);
		}
		return layout;
	}
}

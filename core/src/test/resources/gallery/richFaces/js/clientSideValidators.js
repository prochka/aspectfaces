/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 validate and show first error found in validation

 functions does not support overloading!
 ref: http://weblogs.asp.net/jgalloway/archive/2005/10/02/426345.aspx
 */
;
function Const() {
};

Const.MSG_DELIMITER = ';';
Const.MSG_ARG = '{0}';
/* default messages */
Const.MSG_REQUIRED = "required";
Const.MSG_DATE = 'date invalid';
Const.MSG_FUTURE = 'has to be future';
Const.MSG_PAST = 'has to be past';
Const.MSG_NUMBER = 'not a number';
Const.MSG_MIN_RANGE = 'min range: {0}';
Const.MSG_MAX_RANGE = 'max range: {0}';
Const.MSG_PATTERN = 'pattern: {0}';
Const.MSG_MAX_LENGTH = 'max length: {0}';
Const.MSG_MIN_LENGTH = 'min length: {0}';
Const.MSG_EMAIL = 'email';
Const.MSG_PASSWORD = 'Passwords do not match';
Const.MSG_PASSWORD_NOSPACE = 'Password cannot start or end with a space';
Const.MSG_TOTAL = '"Please update all {0} highlighted errors"';

/* index in messages
 // date*/
Const.I_REQUIRED = 0;
Const.I_D_DATE = 1;
Const.I_D_FUTURE = 2;
Const.I_D_PAST = 3;
/*
 // number*/
Const.I_N_NUMBER = 1;
Const.I_N_MIN_RANGE = 2;
Const.I_N_MAX_RANGE = 3;
Const.I_N_PATTERN = 4;
/*    
 // text*/
Const.I_T_MIN_LENGTH = 1;
Const.I_T_MAX_LENGTH = 2;
Const.I_T_PATTERN = 3;
Const.I_T_EMAIL = 4;

var registerField = new Array();

var errorsTotal = 0;
/*
 //http://www.hunlock.com/blogs/Mastering_Javascript_Arrays#quickIDX31*/
/* save existing warnings for global message */
var arrayMessage = new Array();
var arrayComponent = new Array();


/**
 * this is because of JSF COLORs
 * color has odd validation 3 at the time
 */
function alreadyQueued(componentId) {
    for (i = 0; i < arrayComponent.length; ++i) {
        if (arrayComponent[i] == componentId) {
            return true;
        }
    }
    return false;
}

/**
 * this is because of JSF COLORs
 * use one component!
 */
function checkColor(componentId) {
    var lastIdPart = componentId.lastIndexOf(':') + 1;

    if (componentId.length > 3 && componentId.substring(lastIdPart, lastIdPart + 4) == "red_") {
        return componentId.substring(lastIdPart + 4, componentId.length);
    } else if (componentId.length > 4 && componentId.substring(lastIdPart, lastIdPart + 5) == "blue_") {
        return componentId.substring(lastIdPart + 5, componentId.length);
    } else if (componentId.length > 5 && componentId.substring(lastIdPart, lastIdPart + 6) == "green_") {
        return componentId.substring(lastIdPart + 6, componentId.length);
    }
    return componentId;
}


function addTotalMsg(componentId, message) {
    incErrorsTotal();

    componentId = checkColor(componentId);
    /* colors JSF */

    if (!alreadyQueued(componentId)) { /* colors JSF */
        arrayMessage.push(message);
        arrayComponent.push(componentId);
    }

}
function removeTotalMsg(componentId) {
    decErrorsTotal();

    componentId = checkColor(componentId);
    /* colors JSF */

    var i;
    for (i = 0; i < arrayComponent.length; ++i) {
        /* password has 2 fields - end "password" */
        if (arrayComponent[i] == componentId
            || arrayComponent[i] == componentId + "Confirm") {
            arrayComponent.splice(i, 1);
            arrayMessage.splice(i, 1);
            /*break; */
        } else if (componentId.length > "Confirm".length
            /* - end "passwordConfirm"*/
            && arrayComponent[i] == componentId.substring(0, componentId.length - "Confirm".length)) {
            arrayComponent.splice(i, 1);
            arrayMessage.splice(i, 1);
        }
    }
}

function getErrorsTotal() {
    return errorsTotal;
}

function incErrorsTotal() {
    ++errorsTotal;
}
function decErrorsTotal() {
    --errorsTotal;
}

function isIE() {
    if (navigator.appName.indexOf("Microsoft") != -1
        || navigator.appName.indexOf("MSIE") != -1
        || navigator.appVersion.indexOf("MSIE") != -1) {

        return true;
    } else {
        return false;
    }
    /*
     // Opera
     // Netscape
     */
}

/*private*/
function checkAllFormFieldByType(component, type) {
    var inputs = component.parentNode.parentNode.getElementsByTagName(type.toUpperCase());
    if (inputs.length == 0) {
        inputs = component.parentNode.parentNode.getElementsByTagName(type);
    }
    var i;
    for (i = 0; i < inputs.length; ++i) {
        if (inputs[i].type != 'hidden' && inputs[i].type != 'HIDDEN'
            && inputs[i].style.display != "none" && inputs[i].style.display != "NONE"
            && inputs[i].type != "submit" && inputs[i].type != "SUBMIT") {
            inputs[i].focus();
            inputs[i].blur();
            if (isIE()) {
                /* TODO other events */
                inputs[i].fireEvent("onblur");
                /*inputs[i].fireEvent("onclick"); // nope! */
            }
        }
        /*window.focus();*/
    }
}

function allowSubmitCheckAllNoAlert(component) {
    checkAllFormFieldByType(component, "input");
    checkAllFormFieldByType(component, "textarea");
    if (errorsTotal > 0) {
        return false;
    } else {
        return true;
    }
}
function allowSubmitCheckAll(component, message) {
    allowSubmitCheckAllNoAlert(component)

    return allowSubmit(message);
}

function customDragDropCheckAll(component, message) {
    var id = component.parameters.dropTargetId;
    var prefix = id.substring(0, id.lastIndexOf(':'));
    var newComp = document.getElementById(prefix);
    return allowSubmitCheckAll(newComp, message);
}

function allowSubmit(message) {
    var outMessage = (message == null) ? Const.MSG_TOTAL : message;

    if (errorsTotal > 0) {
        outMessage = outMessage.replace(/\{0\}/, errorsTotal + "");
        if (arrayMessage.length > 0) {
            /* show messages */
            var i;
            for (i = 0; i < arrayMessage.length; ++i) {
                outMessage += "\n" + arrayComponent[i] + " =>" + arrayMessage[i];
            }
        }
        alert(outMessage);
        return false;
    } else {
        return true;
    }
}


/*
 Internationalization 

 in message bundle from JSP or JSF use

 #client side validator
 #Required;Data;Future;Past
 client.validator.date=required;date malformation;has to be future;has to be past
 #Required;Number;MinRange;MaxRange
 client.validator.number=required;not a number;min range: {0};max range: {0};pattern: {0}
 #Required;MinLen;MaxLen;Pattern;Email;Pattern
 client.validator.text=required;min length: {0}; max length: {0};pattern: {0};email
 #regexp
 /^([a-zA-Z0-9_.+-])+@$/=word with @ at the end

 */

/* date validator with optional messageBungle and patternMessage*/
/*function validateInputDate(component, required, future, past)  */
function validateInputDate(component, required, future, past, messageBundle) {
    /* handle rich date */
    if (component.tagName == "table" || component.tagName == "TABLE") {
        component = document.getElementById(component.id + "InputDate")
    }

    eraseError(component);

    /*not required and empty ""*/
    if (isNotReqiredAndEmpty(component, required)) {
        return;
    }

    /* set messages to null*/
    var msg = new Array(4);
    if (messageBundle != null && messageBundle.length != 0) {
        msg = parseMessageBundle(messageBundle);
    }

    if (!checkRequired(component, required, msg[Const.I_REQUIRED])) {
        return;
    }
    if (!checkDate(component, msg[Const.I_D_DATE])) {
        return;
    }
    if (!checkFuture(component, future, msg[Const.I_D_FUTURE])) {
        return;
    }
    if (!checkPast(component, past, msg[Const.I_D_PAST])) {
        return;
    }
}

/* number validator with optional messageBungle and patternMessage*/
function validateInputNumber(component, required, minRange, maxRange, pattern, messageBundle, patternMessage) {
    /* TODO: future parameter number type [byte,short,int,long,float,double]
     //       also check type octal, decimal and hexa - this may be eaten by richFaces spinbox
     //       wait to get feedback from users if they really want it
     */
    eraseError(component);

    /*not required and empty ""*/
    if (isNotReqiredAndEmpty(component, required)) {
        return;
    }

    var msg = new Array(5);
    if (messageBundle != null && messageBundle.length != 0) {
        /* 6 parameters*/
        msg = parseMessageBundle(messageBundle);
    }
    if (patternMessage != null && patternMessage.length != 0) {
        /* 7 parameters*/
        msg[Const.I_N_PATTERN] = patternMessage;
    }

    if (!checkRequired(component, required, msg[Const.I_REQUIRED])) {
        return;
    }
    if (!checkNumber(component, msg[Const.I_N_NUMBER])) {
        return;
    }
    if (!checkMinRange(component, minRange, msg[Const.I_N_MIN_RANGE])) {
        return;
    }
    if (!checkMaxRange(component, maxRange, msg[Const.I_N_MAX_RANGE])) {
        return;
    }
    if (!checkPattern(component, pattern, msg[Const.I_N_PATTERN])) {
        return;
    }
}

/* text validator with optional messageBungle and patternMessage */
function validateInputText(component, required, minLength, maxLength, email, pattern, messageBundle, patternMessage) {

    eraseError(component);

    /*not required and empty ""*/
    if (isNotReqiredAndEmpty(component, required)) {
        return;
    }

    var msg = new Array(5);
    if (messageBundle != null && messageBundle.length != 0) {
        /* 7 parameters */
        msg = parseMessageBundle(messageBundle);
    }
    if (patternMessage != null && patternMessage.length != 0) {
        /* 8 parameters */
        msg[Const.I_T_PATTERN] = patternMessage;
    }

    if (!checkRequired(component, required, msg[Const.I_REQUIRED])) {
        return;
    }
    if (!checkMinLen(component, minLength, msg[Const.I_T_MIN_LENGTH])) {
        return;
    }
    if (!checkMaxLen(component, maxLength, msg[Const.I_T_MAX_LENGTH])) {
        return;
    }
    if (!checkPattern(component, pattern, msg[Const.I_T_PATTERN])) {
        return;
    }
    if (!checkEmail(component, email, msg[Const.I_T_EMAIL])) {
        return;
    }
}

function validateSelect(component, required, messageBundle) {
    eraseError(component);

    /*not required and empty ""*/
    if (isNotReqiredAndEmpty(component, required)) {
        return;
    }
    var msg = new Array(5);
    if (messageBundle != null && messageBundle.length != 0) {
        /* 7 parameters */
        msg = parseMessageBundle(messageBundle);
    }
    if (!checkRequired(component, required, msg[Const.I_REQUIRED])) {
        return;
    }
}

/**
 * validation color (JSF) on form submit
 * bypass other validation
 */
function validateColors(component, id, messageBundle) {
    var prefix = component.id.substring(0, component.id.lastIndexOf(':'));
    /*var prefix = parseParentTree(component.id);*/
    var red = document.getElementById(prefix + ':red_' + id);
    var green = document.getElementById(prefix + ':green_' + id);
    var blue = document.getElementById(prefix + ':blue_' + id);

    var msg = new Array(5);
    if (messageBundle != null && messageBundle.length != 0) {
        /* 6 parameters the same as number*/
        msg = parseMessageBundle(messageBundle);
    }

    eraseError(component);

    if (!validateOneColor(red, msg)) {
        return;
    }
    if (!validateOneColor(green, msg)) {
        return;
    }
    if (!validateOneColor(blue, msg)) {
        return;
    }
}

function validateOneColor(component, msg) {


    if (!checkNumber(component, msg[Const.I_N_NUMBER])) {
        return false;
    }
    if (!checkMinRange(component, 0, msg[Const.I_N_MIN_RANGE])) {
        return false;
    }
    if (!checkMaxRange(component, 255, msg[Const.I_N_MAX_RANGE])) {
        return false;
    }
    return true;
}

/*
 * Toggle ID element
 */
function getAbsoluteId(component, relativeId) {
    var id = component.id;
    var lastIndex = id.lastIndexOf(':');
    return id.substring(0, lastIndex) + ":" + relativeId;
}

function trimAbsolueId(component, textToTrim) {
    var id = component.id;
    var lenToTrim = textToTrim.length;
    if (id.length > lenToTrim) {
        return id.substring(0, id.length - lenToTrim);
    } else {
        return "Error trimming absoluteId";
    }


}


/**
 * parses message bundle
 */
function parseMessageBundle(messageBundle) {
    return messageBundle.split(Const.MSG_DELIMITER);
}

/**
 * check if evaluated value is "" and not required
 */
function isNotReqiredAndEmpty(component, required) {
    if (required == false && component.value.toString() == "") {
        return true;
    } else {
        return false;
    }
}

function checkNumber(component, message) {
    var outMessage = (message == null) ? Const.MSG_NUMBER : message;

    if (isNaN(parseInt(component.value)) && isNaN(parseFloat(component.value))) {
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkDate(component, message) {
    var outMessage = (message == null) ? Const.MSG_DATE : message;
    /* Opera converts 1t/e2/2006 to date ;D */
    /*
     * Date pattern yyyy-mm-dd: (19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])
     * Date pattern mm/dd/yyyy: (0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d
     * Date pattern mm/dd/yyyy hh:mm: (0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d[ ]\d\d[:]\d\d
     * Date pattern dd-mm-yyyy: (0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d
     */

    var datePattern = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    /*var datePattern = /^\d\d\/\d\d\/\d\d\d\d$/;*/
    /*var dateTimePattern = /^\d\d\/\d\d\/\d\d\d\d[ ]\d\d[:]\d\d$/;*/
    var dateTimePattern = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d[ ]\d\d[:]\d\d$/;
    var matchDate = new RegExp(datePattern);
    var matchDateTime = new RegExp(dateTimePattern);


    if (matchDate.test(component.value) == false && matchDateTime.test(component.value) == false) {
        /*   if(isNaN(Date.parse(component.value))) {*/
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkPast(component, past, message) {
    var outMessage = (message == null) ? Const.MSG_PAST : message;


    if (past && Date.parse(component.value) > Date.parse(Date())) {
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkFuture(component, future, message) {
    var outMessage = (message == null) ? Const.MSG_FUTURE : message;

    if (future && Date.parse(component.value) < Date.parse(Date())) {
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkMaxRange(component, maxRange, message) {
    var outMessage = (message == null) ? Const.MSG_MAX_RANGE : message;

    if (maxRange.length != 0 && parseInt(component.value) > parseInt(maxRange)) {
        outMessage = outMessage.replace(/\{0\}/, maxRange + "");
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkMinRange(component, minRange, message) {
    var outMessage = (message == null) ? Const.MSG_MIN_RANGE : message;

    if (minRange.length != 0 && parseInt(component.value) < parseInt(minRange)) {
        outMessage = outMessage.replace(/\{0\}/, minRange + "");
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkMinLen(component, minLen, message) {
    var outMessage = (message == null) ? Const.MSG_MIN_LENGTH : message;

    if (minLen.length != 0 && component.value.length < parseInt(minLen)) {

        outMessage = outMessage.replace(/\{0\}/, minLen + "");
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkMaxLen(component, maxLen, message) {
    var outMessage = (message == null) ? Const.MSG_MIN_LENGTH : message;

    if (maxLen.length != 0 && component.value.length > parseInt(maxLen)) {
        outMessage = outMessage.replace(/\{0\}/, maxLen + "");
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkRequired(component, required, message) {
    var outMessage = (message == null) ? Const.MSG_REQUIRED : message;

    if (required && (component.value.length == 0 || "org.jboss.seam.ui.NoSelectionConverter.noSelectionValue" == component.value)) {
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkEmail(component, email, message) {
    var outMessage = (message == null) ? Const.MSG_EMAIL : message;

    var pattern = /^([a-zA-Z0-9_\.\+\-])+@(([a-zA-Z0-9\-])+.)+([a-zA-Z0-9]{2,6})+$/;
    var match = new RegExp(pattern);

    if (email && match.test(component.value) == false) {

        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

function checkPattern(component, pattern, message) {
    var outMessage = (message == null) ? Const.MSG_PATTERN : message;

    if (pattern.length == 0) {
        return true;
    }

    var match = new RegExp(pattern);

    if (match.test(component.value) == false) {
        outMessage = outMessage.replace(/\{0\}/, pattern + "");
        addError(component, outMessage);
        return false;
    } else {
        return true;
    }
}

/**
 tests if component invoking this session is in table
 */
function checkIfTagIsInTable(component) {
    /* firefox can hangle 'td' but IE and Opera knows only uppercase 'TD' */
    /* This hangles Tags in Table a richFaces spanbox*/
    if (component.parentNode.tagName == "td" || component.parentNode.tagName == "TD") { /* td tr tbody table span.value*/
        return component.parentNode.parentNode.parentNode.parentNode;
    } else {
        return component;
    }
}


/**
 * span wrapping value has id="value" (FIX if)
 * find it in DOM bubble up
 */
function findValue(component) {
    var relativeComponent = component;
    while (relativeComponent.parentNode.id != "value" && relativeComponent.parentNode.id != "VALUE") {
        relativeComponent = relativeComponent.parentNode;
    }
    return relativeComponent
}

/**
 erase one error message
 */
function eraseError(component) {
    var relativeComponent = component;


    /* This hangles Tags in Table a richFaces spanbox */
    relativeComponent = checkIfTagIsInTable(component);

    /* span wrapping value has id="value" (FIX if)*/
    relativeComponent = findValue(relativeComponent);


    relativeComponent.parentNode.className = relativeComponent.parentNode.className.replace(/ errors/, "");
    /* span.value*/
    relativeComponent.parentNode.parentNode.firstChild.className = relativeComponent.parentNode.parentNode.firstChild.className.replace(/ errors/, "");
    /*// label.name*/


//   for (var i=0; i < relativeComponent.parentNode.childNodes.length; ++i) {
    for (var i = 0; i < relativeComponent.parentNode.parentNode.childNodes.length; ++i) {
        //       var element = relativeComponent.parentNode.childNodes[i];
        var element = relativeComponent.parentNode.parentNode.childNodes[i];

        /* firefox can hangle 'div' but IE and Opera knows only uppercase 'DIV' */
        if ((element.tagName == "div" || element.tagName == "DIV") && element.className.toLowerCase().match(/error/)) {
            if (element.className.match(/ serverError/)) {
                /* it was server error so do not count it*/
                element.className = element.className.replace(/ serverError/, "");
            } else {
                removeTotalMsg(component.id);
            }

            element.parentNode.removeChild(element);
        }
    }
    ;
}

/**
 add class error to the structure
 and add div span with error message
 */
function addError(component, text) {
    var relativeComponent = component;

    addTotalMsg(component.id, text);

    /* This hangles Tags in Table a richFaces spanbox */
    relativeComponent = checkIfTagIsInTable(component);

    /* span wrapping value has id="value" (FIX if) */
    relativeComponent = findValue(relativeComponent);

    relativeComponent.parentNode.className += " errors";
    /* span.value*/
    relativeComponent.parentNode.parentNode.firstChild.className += " errors";
    /* label.name*/

    /*
     <div>
     <span>
     Error
     </span>
     <div>
     */
    var errorText = document.createTextNode(text);
    var div = document.createElement('div');
    var span = document.createElement('span');
    div.className = "errorElement";
    span.appendChild(errorText);
    span.className = "error errors";
    div.appendChild(span);

    var nodeToConnect = relativeComponent.parentNode.parentNode;
    // relativeComponent.parentNode.appendChild(div);
//   if (nodeToConnect.lastChild.previousSibling.className == "tooltipWrap") {
//	   nodeToConnect.insertBefore(div,nodeToConnect.lastChild.previousSibling);
//   } else {
//	   nodeToConnect.appendChild(div);
//   }
    nodeToConnect.appendChild(div);
}

function timeSlide(value, invoker, objectID, type) {
    var invokerName;

    if (invoker.nodeName != null) { /* simple */
        invokerName = (invoker.nodeName.toLowerCase() == "input") ? invoker.id : "undefined";
    } else { /* seam rich */
        invokerName = invoker.input.name;

    }


    var prefix = parseParentTree(invokerName);
    var object = document.getElementById(prefix + ':' + objectID);

    if (object != null) {
        /* get existing color and add new */
        var newValue = ""
        if (object.value == null || object.value == "") {
            var currentTime = new Date();
            var month = (currentTime.getMonth() + 1);
            month = (month <= 9) ? "0" + month : month;
            var day = currentTime.getDate();
            day = (day <= 9) ? "0" + day : day;
            var year = currentTime.getFullYear();

            newValue = month + "/" + day + "/" + year;

        }
        if (object.value.length >= 10) {
            newValue = object.value.substr(0, 10);
        }
        newValue += " ";

        if (object.value.length > 11) {
            var hour = object.value.substr(11, 2);
            var minute = object.value.substr(14, 2);
        } else {
            var hour = "00";
            var minute = "00";

        }

        if (value <= 9) {
            value = "0" + value;
        }
        if (type == "hour") {
            newValue += value + ":";
            newValue += minute;

        } else {
            newValue += hour + ":";
            newValue += value;
        }

        object.value = newValue;
    }
}

